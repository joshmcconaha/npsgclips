---
title: Clips
date: 2018-06-12 20:47:44 +0000
report_title: Facebook Coverage 6/12/18
report_date: 2018-06-12 04:00:00 +0000
article:
- template: article
  title: The Trump appointee making Silicon Valley sweat
  publication: Politico
  url: https://www.politico.com/story/2018/06/11/trump-antitrust-chief-silicon-valley-608012
  publication_date: 2018-06-11 04:00:00 +0000
  authors:
  - author: Nancy Scola
  full_text: |-
    One of President Donald Trump’s top regulators is raising the temperature on Silicon Valley.

     

    Makan Delrahim, who heads the Justice Department’s antitrust division, has spent months laying out a case for greater scrutiny of the country’s powerful technology industry, making the argument in speeches from Chicago to Rome.

     

    He faces an early test Tuesday when a federal judge rules on his first big action, a lawsuit to block the $85 billion merger between AT&T and Time Warner. The Trump administration’s argument, that the deal could stifle competition and raise prices for consumers, is a departure from the traditional GOP hands-off approach to business — and Delrahim is making it clear that the tech industry’s practices are under his microscope as well.

     

    “At the end of the day, my job is policing the free market system to make sure you have competition to create innovation and ultimately help consumers," he said in an interview Monday with POLITICO.

     

    Delrahim’s stance is the latest example of a Trump administration willing to upend Republican orthodoxy on everything from trade and tariffs to propping up the coal industry.

     

    And his rhetoric — he told an audience at the University of Chicago in April that "enforcers must take vigorous action" if digital platforms harm competition — is being closely watched in the tech industry amid fears that Washington's souring view on Silicon Valley could eventually result in a crackdown.

     

    The industry was once the darling of both parties. But it has become a punching bag of late, as Trump regularly lambastes Amazon, Facebook grapples with controversies over user privacy and Russian election interference, and conservatives allege social media are biased against their views.

     

    Delrahim’s talk is raising hopes among some progressives that they have an unlikely ally in the Trump administration. Still, those agitating for a tougher approach to tech companies say they’re not clear whether the antitrust chief is planning to take action — or simply floating ideas.

     

    “We're still waiting to see whether, under his leadership, the antitrust division will use its significant investigatory tools and enforcement tools to address those concerns head on,” said Lina Khan, director of legal policy for the Open Markets Institute, which advocates for checks on the tech sector.

     

    Trump’s opposition to the AT&T-Time Warner deal and his animosity toward Time Warner-owned CNN, which he accuses of spreading “fake news," has loomed over the merger trial, leading critics to suggest the Justice Department is taking a politically motivated approach to the case.

     

    Delrahim takes umbrage at such suggestions, saying they're untrue and ginned up in part by AT&T.

     

    “Politics plays no role in our law enforcement of antitrust cases. It doesn't, it shouldn't, and it won’t," he said.

     

    Asked point-blank if Trump’s opinions influenced his decision to bring a lawsuit to block the AT&T deal, Delrahim responded, “No, of course not.”

     

    While AT&T-Time Warner would combine two companies that don’t compete directly — a so-called vertical merger that has passed muster with regulators in the past — Delrahim has outlined a different approach. He’s shown a preference for addressing competition concerns by either requiring companies to restructure or blocking such deals altogether.

     

    He’s a harsh critic of government-imposed conditions on deals dictating specific business practices, saying they cause regulators to “start veering into trying to second guess what the market will be and what consumers will want five years, seven years, 10 years down the line."

     

    In his decades in Washington — as a Senate staffer, DOJ official in the Bush administration, lawyer in the private sector and later an aide to Trump in the White House — Delrahim has developed a reputation of being eager to debate ideas in public, on occasion taking the unexpected view.

     

    “He’s willing to stir the pot. He thinks independently. And he will surprise people,” says former Democratic FTC Chairman Jon Leibowitz, a partner at the law firm Davis Polk & Wardwell who served with Delrahim as a staffer on the Senate Judiciary Committee in the late 1990s.

     

    Delrahim argues that his job isn’t to pick winners or losers in the market, but to police the private sector so that consumers get what they want and government won’t be moved to impose suffocating regulations on it.

     

    He’s floated new metrics for judging whether an online company is good for consumers, arguing that traditional factors like low pricing are less relevant in the age of free services like Gmail and Facebook. One possible metric, he says, are the scores used by marketers to determine the likelihood of users recommending a product to a friend.

     

    When it comes to Facebook, Delrahim said he has concerns about its handling of user data — “There’s no question I care about my personal privacy. I think most consumers do, if they actually understand it” — but said the company’s behavior isn’t necessarily anti-competitive.

     

    The way for his office to promote privacy among online services, he said, is to make sure there’s a clear path for Snapchat or another competitor to offer a Facebook alternative for consumers who place a priority on privacy and "the market is free for such a thing to develop.“

     

    Despite his growing profile in the tech industry, Delrahim cautioned that the Justice Department only brings cases when there’s evidence to back them up.

     

    "It’s really easy to be out there telling people to go to hell. Getting them is a whole different story, and that’s my job," he said, paraphrasing Jack Valenti, the legendary head of the Motion Picture Association of America. "You have to have the credible evidence, a sound economic theory, and you have to have a case you can win. Otherwise everything goes to dust."

     
- template: article
  title: " Fair Play: Rupert Murdoch Could Get Sky Like He Wants, Sort Of"
  publication: Bloomberg
  url: https://www.bna.com/fair-play-rupert-b73014476369/
  publication_date: 2018-06-10 04:00:00 +0000
  authors:
  - author: Fawn Johnson
  full_text: |-
    The U.K. media industry and Rupert Murdoch have a long history. Murdoch bought British tabloid newspapers News of the World and The Sun in 1969 and founded the British satellite TV network Sky Television in 1989, which merged with British Satellite Television a year later.

     

    He helped turned the flailing Sun and Sky PLC, of which he owns 39 percent, into successes. He’s a major figure in British media.

     

    So it was no surprise that last week, Culture Secretary Matt Hancock went before the British Parliament to announce the decision to allow Murdoch’s 21st Century Fox to buy the remaining part of Sky. The way he said it – “I can confirm today that I will not be issuing an intervention notice” – hinted at the tortured road the deal has taken.

     

    The transaction has been scrutinized intensely by the U.K.’s competition authority and its communications regulator since late 2016 over an array of questions regarding consolidation and editorial choices. The deal hit more snags late last year when reports of sexual harassment at Fox News were unveiled, causing the competition authority to question how Sky employees would be treated.

     

    But in the end, it came down to control over the news. The U.K. says Murdoch, who already owns News Corp., can’t have another news entity. The regulators say the deal can only go through if the merged Fox-Sky entity sells the 24-hour news channel Sky News, now part of Sky PLC. If the Sky News sale doesn’t happen, the only other option the government has would be blocking the merger.

     

    The U.K. communications regulator said the Murdoch Family Trust already controls a fair amount of British media. The Murdochs also own London newspapers The Times, and The Sun on Sunday, and The Sunday Times.

     

    If Sky News were to fall under the Murdoch umbrella, the regulator said, the threat to media is two-fold. First, the shared ownership could result in “a reduction in the diversity of viewpoints consumed by the public.” Second, that reduction in diverse viewpoints could increase the Murdochs’ ability to “influence public opinion and the political agenda.”

     

    The question about what happens to the diversity and the availability of content is a central part of media merger inquiries in both the U.S. and the U.K.  Unlike non-media mergers, where questions about consolidation tend to revolve around price effects or the number of competitors, regulators reviewing media deals consider actual content – particularly if it is news – to be of value in and of itself. Without access to it, consumers suffer. And without diversity in viewpoints, the thinking goes, democracy suffers.

     

    The U.K. competition authority delved deeply into the question of what would happen to editorial content in the wake of a Fox-Sky deal, asking how people consumed news, and through what sources. It noted that media diets are changing with online news platforms and intermediaries like Facebook and Twitter that aggregate the news.

     

    But when it came down to it, the U.K. regulators concluded that the risk of losing diversity in content was too great. A combined Sky-News Corp. entity would reach 31 percent of British readers, third behind the BBC and Independent Television news.

     

    The Murdochs have agreed to sell Sky News to get Sky PLC, but they aren’t out of the woods yet on the deal. They are fighting Comcast Corp., which is expected to make a competing bid for Sky this week. Hancock has already said the U.K. government won’t intervene in that deal if it goes forward.

     

    What’s Happening

    On Monday, the Senate is expected to begin debate on a defense bill that includes an expansion of the authority of the Committee on Foreign Investment in the U.S., or CFIUS.

     

    On Tuesday, U.S. District Court for the District of Columbia Judge Richard Leon is expected to deliver his decision on whether the merger between AT&T Inc. and Time Warner Inc. can go forward.

     

    Also on Tuesday, Justice Department antitrust chief Makan Delrahim is will give remarks at an Open Markets Institute event on antitrust issues in the media. Sen. Amy Klobuchar (D-Minn.) and Rep. David Cicciline (D-R.I.), both active on antitrust issues, will also speak at the all-day conference.

     

    On Wednesday, Delrahim will give remarks at the National Association of Music Publisher’s annual meeting in New York.
article_3: []
---
